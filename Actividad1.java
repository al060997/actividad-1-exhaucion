package actividad1;

import javax.swing.JOptionPane;

public class Actividad1 {

    public static void main(String[] args) {
        
        int nlados = 6;
        double ladoinicial = 1;
        double x = 0;
        double a = 0;
        double b = 0;
        double nuevolado = 0;
        double P = 0;
        double PD = 0;
        int A = 0;
        
        for(int i = 1; i < 13; i++){
            if(i == 1){
                x = (ladoinicial/2);
                double X = x*x;
                a = Math.sqrt(1-(X));
                b = (1-a);
                double B = b*b;
                nuevolado = Math.sqrt((X)+(B));
                P = nlados*ladoinicial;
                PD = P/2;
                
                System.out.println("N lados = " + nlados);
                System.out.println("Lado inical = " + ladoinicial);
                System.out.println("x = " + x);
                System.out.println("a = " + a);
                System.out.println("b = " + b);
                System.out.println("Nuevo lado = " + nuevolado);
                System.out.println("P = " + P);
                System.out.println("P/D = " + PD);
            }  
            if(i >= 2){
                nlados = (nlados*2);
                ladoinicial = nuevolado;
                x = (ladoinicial/2);
                double X = x*x;
                a = Math.sqrt(1-(X));
                b = (1-a);
                double B = b*b;
                nuevolado = Math.sqrt((X)+(B));
                P = nlados*ladoinicial;
                PD = P/2;
                
                System.out.println("N lados = " + nlados);
                System.out.println("Lado inical = " + ladoinicial);
                System.out.println("x = " + x);
                System.out.println("a = " + a);
                System.out.println("b = " + b);
                System.out.println("Nuevo lado = " + nuevolado);
                System.out.println("P = " + P);
                System.out.println("P/D = " + PD);
            }
        }
        
    }
    
}
